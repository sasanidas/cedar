(eval-when (:compile-toplevel)
  (error "This ASDF file should be run interpreted."))

(defsystem "cedar"
  :depends-on (:alexandria
	       :cl-ppcre
	       :uiop
	       :croatoan
	       :slynk)
  :serial t
  :components ((:module core
		:serial t
		:components ((:file "package")
			     (:file "wrappers")
			     (:file "emacs")
			     (:file "callproc")
			     (:file "parser/elisp")
			     (:file "global")
			     (:file "fns")
			     (:file "data")
			     (:file "custom")
			     (:file "commands")
			     (:file "callint")
			     (:file "dired")
			     (:file "data-types")
			     (:file "charset")
			     (:file "subprocesses")
			     (:file "buffer-local")
			     (:file "keymap")
			     (:file "casefiddle")
			     (:file "buffer")
			     (:file "intervals")
			     (:file "textprop")
			     (:file "search")
			     (:file "frame")
			     (:file "window")
			     (:file "render")
			     (:file "wm")

			     ;; from this point on there are warnings because of two-way dependencies
			     (:file "insdel")
			     (:file "cmds")
			     (:file "editfns")
			     (:file "undo")
			     (:file "syntax")
			     (:file "major-mode")
			     (:file "keyboard")
			     (:file "debugger")
			     (:file "recursive-edit")
			     (:file "minibuffer")
			     (:file "files")
			     (:file "help")
			     (:file "debug")
			     (:file "indent")
			     (:file "fileio")))
	       (:module UI
		:serial t
		:components ((:file "tty-cro")))

	       (:module modes
		:serial t
		:components ((:file "easy-mmode")
			     (:file "lisp-mode")
			     (:file "regex-op")
			     (:file "thingatpt")))

	       (:module lisp
		:serial t
		:components ((:file "subr")
			     (:file "simple")
			     (:file "lisp-indent")
			     (:file "paragraphs")
			     (:file "bindings")
			     ;;(:file "help")
			     ;;(:file "paren")
			     ))

	       (:file "main")))


(defsystem :cedar/tests
  :depends-on (:cedar :fiveam)
  :serial t
  :components ((:module tests
		:serial t
		:components ((:file "test-core")))))
