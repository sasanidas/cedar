
(in-package :cedar)


(defun thing-at-point (thing &optional (no-properties t))
  "Return the THING at point.
THING should be a symbol specifying a type of syntactic entity.
Possibilities include `symbol', `list', `sexp', `defun',
`filename', `url', `email', `uuid', `word', `sentence', `whitespace',
`line', `number', and `page'.

When the optional argument NO-PROPERTIES is non-nil,
strip text properties from the return value.

See the file `thingatpt.el' for documentation on how to define
a symbol as a valid THING."
  (let ((text
	  (if (get thing 'thing-at-point)
	      (funcall (get thing 'thing-at-point))
	      (let ((bounds (bounds-of-thing-at-point thing)))
		(when bounds
		  (buffer-substring (car bounds) (cdr bounds)))))))
    (when (and text no-properties (sequencep text))
      (set-text-properties 0 (length text) nil text))
    text))

(put 'sexp 'beginning-op 'thing-at-point--beginning-of-sexp)

(put 'line 'beginning-op
     (lambda () (if (bolp) (forward-line -1) (beginning-of-line))))

(defun bounds-of-thing-at-point (thing)
  "Determine the start and end buffer locations for the THING at point.
THING should be a symbol specifying a type of syntactic entity.
Possibilities include `symbol', `list', `sexp', `defun',
`filename', `url', `email', `uuid', `word', `sentence', `whitespace',
`line', and `page'.

See the file `thingatpt.el' for documentation on how to define a
valid THING.

Return a cons cell (START . END) giving the start and end
positions of the thing found."
  (if (get thing 'bounds-of-thing-at-point)
      (funcall (get thing 'bounds-of-thing-at-point))
      (let ((orig (point)))
	(ignore-errors
	 (save-excursion
	     ;; Try moving forward, then back.
	     (funcall ;; First move to end.
	      (or (get thing 'end-op)
		  (lambda () (forward-thing thing 1))))
	   (funcall ;; Then move to beg.
	    (or (get thing 'beginning-op)
		(lambda () (forward-thing thing -1))))
	   (let ((beg (point)))
	     (elisp:if (<= beg orig)
		       ;; If that brings us all the way back to ORIG,
		       ;; it worked.  But END may not be the real end.
		       ;; So find the real end that corresponds to BEG.
		       ;; FIXME: in which cases can `real-end' differ from `end'?
		       (let ((real-end
			       (progn
				 (funcall
				  (or (get thing 'end-op)
				      (lambda () (forward-thing thing 1))))
				 (point))))
			 (when (and (<= orig real-end) (< beg real-end))
			   (cons beg real-end)))
		       (goto-char orig)
		       ;; Try a second time, moving backward first and then forward,
		       ;; so that we can find a thing that ends at ORIG.
		       (funcall ;; First, move to beg.
			(or (get thing 'beginning-op)
			    (lambda () (forward-thing thing -1))))
		       (funcall ;; Then move to end.
			(or (get thing 'end-op)
			    (lambda () (forward-thing thing 1))))
		       (let ((end (point))
			     (real-beg
			       (progn
				 (funcall
				  (or (get thing 'beginning-op)
				      (lambda () (forward-thing thing -1))))
				 (point))))
			 (if (and (<= real-beg orig) (<= orig end) (< real-beg end))
			     (cons real-beg end))))))))))

(defun forward-thing (thing &optional n)
  "Move forward to the end of the Nth next THING.
THING should be a symbol specifying a type of syntactic entity.
Possibilities include `symbol', `list', `sexp', `defun',
`filename', `url', `email', `uuid', `word', `sentence', `whitespace',
`line', and `page'."
  (let ((forward-op (or (get thing 'forward-op)
			(intern-soft (format nil "forward-~a" thing)))))
    (if (functionp forward-op)
	(funcall forward-op (or n 1))
	(message  (format nil "Can't determine how to move over a ~a" thing)))))

(defun symbol-at-point ()
  "Return the symbol at point, or nil if none is found."
  (let ((thing (thing-at-point 'symbol)))
    (if thing (intern thing))))

(defun thing-at-point--read-from-whole-string (str)
  "Read a Lisp expression from STR.
Signal an error if the entire string was not used."
  (let* ((read-data (read-from-string str))
	 (more-left
	   (ignore-errors
	    (read-from-string (substring str (cdr read-data))))))
    (princ more-left)
    (if more-left
	(message "Can't read whole string")
	read-data)))

(defun form-at-point (&optional thing pred)
  (let* ((obj (thing-at-point (or thing 'sexp)))
         (sexp (if (stringp obj)
                   (ignore-errors
		    (thing-at-point--read-from-whole-string obj))
		   obj)))
    (if (or (not pred) (funcall pred sexp)) sexp)))

(defun sexp-at-point ()
  "Return the sexp at point, or nil if none is found."
  (form-at-point 'sexp))

(defun number-at-point ()
  "Return the number at point, or nil if none is found.
Decimal numbers like \"14\" or \"-14.5\", as well as hex numbers
like \"0xBEEF09\" or \"#xBEEF09\", are recognized."
  (when (thing-at-point-looking-at "\\(-?[0-9]+\\.?[0-9]*\\)\\|\\(0x\\|#x\\)\\([a-zA-Z0-9]+\\)" 500)
    (match-beginning 1)))

(defun thing-at-point-looking-at (regexp &optional distance)
  "Return non-nil if point is in or just after a match for REGEXP.
Set the match data from the earliest such match ending at or after
point.

Optional argument DISTANCE limits search for REGEXP forward and
back from point."
  (ignore-errors
   (save-excursion
       (let ((old-point (point))
	     (forward-bound (and distance (+ (point) distance)))
	     (backward-bound (and distance (- (point) distance)))
	     match prev-pos new-pos)
	 (and (looking-at regexp)
	      (>= (match-end 0) old-point)
	      (setq match (point)))
	 ;; Search back repeatedly from end of next match.
	 ;; This may fail if next match ends before this match does.
	 (re-search-forward regexp :bound forward-bound :error nil)
	 (setq prev-pos (point))
	 (while (and (setq new-pos (re-search-backward regexp
						       :bound backward-bound
						       :error nil))
		     ;; Avoid inflooping with some regexps, such as "^",
		     ;; matching which never moves point.
		     (< new-pos prev-pos)
		     (or (> (match-beginning 0) old-point)
			 (and (looking-at regexp)	; Extend match-end past search start
			      (>= (match-end 0) old-point)
			      (setq match (point))))))
	 (elisp:if (not match) nil
		   (goto-char match)
		   ;; Back up a char at a time in case search skipped
		   ;; intermediate match straddling search start pos.
		   (while (and (not (bobp))
			       (progn (backward-char 1) (looking-at regexp))
			       (>= (match-end 0) old-point)
			       (setq match (point))))
		   (goto-char match)
		   (looking-at regexp))))))


(provide 'thingatpt)
