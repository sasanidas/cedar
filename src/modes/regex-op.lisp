(in-package :CEDAR)

(defun regexp-opt (strings &optional paren)
  "Return a regexp to match a string in the list STRINGS.
Each member of STRINGS is treated as a fixed string, not as a regexp.
Optional PAREN specifies how the returned regexp is surrounded by
grouping constructs.

If STRINGS is the empty list, the return value is a regexp that
never matches anything.

The optional argument PAREN can be any of the following:

a string
    the resulting regexp is preceded by PAREN and followed by
    \\), e.g.  use \"\\\\(?1:\" to produce an explicitly numbered
    group.

`words'
    the resulting regexp is surrounded by \\=\\<\\( and \\)\\>.

`symbols'
    the resulting regexp is surrounded by \\_<\\( and \\)\\_>.

non-nil
    the resulting regexp is surrounded by \\( and \\).

nil
    the resulting regexp is surrounded by \\(?: and \\), if it is
    necessary to ensure that a postfix operator appended to it will
    apply to the whole expression.

The returned regexp is ordered in such a way that it will always
match the longest string possible.

Up to reordering, the resulting regexp is equivalent to but
usually more efficient than that of a simplified version:

 (defun simplified-regexp-opt (strings &optional paren)
   (let ((parens
          (cond ((stringp paren)       (cons paren \"\\\\)\"))
                ((eq paren \\='words)    \\='(\"\\\\\\=<\\\\(\" . \"\\\\)\\\\>\"))
                ((eq paren \\='symbols) \\='(\"\\\\_<\\\\(\" . \"\\\\)\\\\_>\"))
                ((null paren)          \\='(\"\\\\(?:\" . \"\\\\)\"))
                (t                       \\='(\"\\\\(\" . \"\\\\)\")))))
     (concat (car parens)
             (mapconcat \\='regexp-quote strings \"\\\\|\")
             (cdr parens))))"
  ;; Recurse on the sorted list.
  (let* (
	 (completion-ignore-case nil)
	 (completion-regexp-list nil)
	 (open (cond ((stringp paren) paren) (paren "\\(")))
	 (re (elisp:if strings
		       (regexp-opt-group
			(delete-dups (sort (copy-sequence strings) 'string-lessp))
			(or open t) (not open))
		       ;; No strings: return an unmatchable regexp.
		       (format nil "~a~a~a"
			       (or open "\\(?:")
			       regexp-unmatchable
			       "\\)"))))
    (cond ((eq paren 'words)
	   (concatenate 'string "\\<" re "\\>"))
	  ((eq paren 'symbols)
	   (concatenate 'string "\\_<" re "\\_>"))
	  (t re))))


(defun regexp-opt-group (strings &optional paren lax)
  "Return a regexp to match a string in the sorted list STRINGS.
If PAREN non-nil, output regexp parentheses around returned regexp.
If LAX non-nil, don't output parentheses if it doesn't require them.
Merges keywords to avoid backtracking in Emacs's regexp matcher."
  ;; The basic idea is to find the shortest common prefix or suffix, remove it
  ;; and recurse.  If there is no prefix, we divide the list into two so that
  ;; (at least) one half will have at least a one-character common prefix.

  ;; Also we delay the addition of grouping parenthesis as long as possible
  ;; until we're sure we need them, and try to remove one-character sequences
  ;; so we can use character sets rather than grouping parenthesis.
  (let* ((open-group (cond ((stringp paren) paren) (paren "\\(") (t "")))
	 (close-group (if paren "\\)" ""))
	 (open-charset (if lax "" open-group))
	 (close-charset (if lax "" close-group)))
    (cond
      ;;
      ;; If there are no strings, just return the empty string.
      ((= (length strings) 0)
       "")
      ;;
      ;; If there is only one string, just return it.
      ((= (length strings) 1)
       (if (= (length (car strings)) 1)
	   (concat open-charset (regexp-quote (car strings)) close-charset)
	   (concat open-group (regexp-quote (car strings)) close-group)))
      ;;
      ;; If there is an empty string, remove it and recurse on the rest.
      ((= (length (car strings)) 0)
       (concat open-charset
	       (regexp-opt-group (cdr strings) t t) "?"
	       close-charset))
      ;;
      ;; If there are several one-char strings, use charsets
      ((and (= (length (car strings)) 1)
	    (let ((strs (cdr strings)))
	      (while (and strs (/= (length (car strs)) 1))
		(pop strs))
	      strs))
       (let (letters rest)
	 ;; Collect one-char strings
	 (dolist (s strings)
	   (if (= (length s) 1) (push (string-to-char s) letters) (push s rest)))

	 (if rest
	     ;; several one-char strings: take them and recurse
	     ;; on the rest (first so as to match the longest).
	     (concat open-group
		     (regexp-opt-group (nreverse rest))
		     "\\|" letters
		     close-group)
	     ;; all are one-char strings: just return a character set.
	     (concat open-charset letters close-charset))))
      ;;
      ;; We have a list of different length strings.
      (t
       (let ((prefix (try-completion "" strings)))
	 (if (> (length prefix) 0)
	     ;; common prefix: take it and recurse on the suffixes.
	     (let* ((n (length prefix))
		    (suffixes (mapcar (lambda (s) (substring s n)) strings)))
	       (concat open-group
		       (regexp-quote prefix)
		       (regexp-opt-group suffixes t t)
		       close-group))

	     (let* ((sgnirts (mapcar #'reverse strings))
		    (xiffus (try-completion "" sgnirts)))
	       (if (> (length xiffus) 0)
		   ;; common suffix: take it and recurse on the prefixes.
		   (let* ((n (- (length xiffus)))
			  (prefixes
			    ;; Sorting is necessary in cases such as ("ad" "d").
			    (sort (mapcar (lambda (s) (substring s 0 n)) strings)
				  'string-lessp)))
		     (concat open-group
			     (regexp-opt-group prefixes t t)
			     (regexp-quote (nreverse xiffus))
			     close-group))

		   ;; Otherwise, divide the list into those that start with a
		   ;; particular letter and those that do not, and recurse on them.
		   (let* ((char (substring-no-properties (car strings) 0 1))
			  (half1 (all-completions char strings))
			  (half2 (nthcdr (length half1) strings)))
		     (concat open-group
			     (regexp-opt-group half1)
			     "\\|" (regexp-opt-group half2)
			     close-group))))))))))

(defun regexp-opt-depth (regexp)
  "Return the depth of REGEXP.
This means the number of non-shy regexp grouping constructs
\(parenthesized expressions) in REGEXP."
  (save-match-data
   ;; Hack to signal an error if REGEXP does not have balanced parentheses.
   (string-match regexp "")
   ;; Count the number of open parentheses in REGEXP.
   (let ((count 0) start last)
     (while (string-match "\\\\(\\(\\?[0-9]*:\\)?" regexp start)
       (setq start (match-end 0))	      ; Start of next search.
       (when (and (not (match-beginning 1))
		  (subregexp-context-p regexp (match-beginning 0) last))
	 ;; It's not a shy group and it's not inside brackets or after
	 ;; a backslash: it's really a group-open marker.
	 (setq last start)	    ; Speed up next regexp-opt-re-context-p.
	 (setq count (1+ count))))
     count)))

(provide 'regex-opt)
