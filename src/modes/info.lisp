;; info.lisp --- Info package for Lice 

;; Maintainer: fmfs@posteo.net
;; Keywords: help


(in-package :CEDAR)

(defgroup info nil
  "Info subsystem."
  :group 'help
  :group 'docs)

(define-buffer-local Info-history nil
  "Stack of Info nodes user has visited.
Each element of the stack is a list (FILENAME NODENAME BUFFERPOS).")

(define-buffer-local Info-history-forward nil
  "Stack of Info nodes user has visited with `Info-history-back' command.
Each element of the stack is a list (FILENAME NODENAME BUFFERPOS).")

(defvar Info-history-list nil
  "List of all Info nodes user has visited.
Each element of the list is a list (FILENAME NODENAME).")

(defcustom Info-history-skip-intermediate-nodes t
  "Non-nil means don't record intermediate Info nodes to the history.
Intermediate Info nodes are nodes visited by Info internally in the process of
searching the node to display.  Intermediate nodes are not presented
to the user."
  :type 'boolean
  :group 'info
  :version "24.1")

(defvar Info-enable-active-nodes nil
  "Non-nil allows Info to execute Lisp code associated with nodes.
The Lisp code is executed when the node is selected.")
(put 'Info-enable-active-nodes 'risky-local-variable t)


(defcustom Info-fontify-maximum-menu-size 400000
  "Maximum size of menu to fontify if `font-lock-mode' is non-nil.
Set to nil to disable node fontification; set to t for no limit."
  :type '(choice (const :tag "No fontification" nil)
	  (const :tag "No size limit" t)
	  (integer :tag "Up to this many characters"))
  :version "25.1"			; 100k -> 400k
  :group 'info)

(defcustom Info-use-header-line t
  "Non-nil means to put the beginning-of-node links in an Emacs header-line.
A header-line does not scroll with the rest of the buffer."
  :type 'boolean
  :group 'info)

(defcustom Info-default-directory-list '("/usr/local/" "/usr/" "/opt/")
  "Default list of directories to search for Info documentation files.
They are searched in the order they are given in the list.
Therefore, the directory of Info files that come with Emacs
normally should come last (so that local files override standard ones),
unless Emacs is installed into a non-standard directory.  In the latter
case, the directory of Info files that come with Emacs should be
first in this list.

Once Info is started, the list of directories to search
comes from the variable `Info-directory-list'.
This variable `Info-default-directory-list' is used as the default
for initializing `Info-directory-list' when Info is started, unless
the environment variable INFOPATH is set.

Although this is a customizable variable, that is mainly for technical
reasons.  Normally, you should either set INFOPATH or customize
`Info-additional-directory-list', rather than changing this variable."
  :initialize 'custom-initialize-delay
  :type '(repeat directory)
  :group 'info)

(defvar Info-directory-list
  '("/usr/local/share/info/" "/usr/share/info/" "/usr/local/share/info/")
  "List of directories to search for Info documentation files.
If nil, meaning not yet initialized, Info uses the environment
variable INFOPATH to initialize it, or `Info-default-directory-list'
if there is no INFOPATH variable in the environment, or the
concatenation of the two if INFOPATH ends with a `path-separator'.

When `Info-directory-list' is initialized from the value of
`Info-default-directory-list', and Emacs is installed in one of the
standard directories, the directory of Info files that come with Emacs
is put last (so that local Info files override standard ones).

When `Info-directory-list' is initialized from the value of
`Info-default-directory-list', and Emacs is not installed in one
of the standard directories, the first element of the resulting
list is the directory where Emacs installs the Info files that
come with it.  This is so that Emacs's own manual, which suits the
version of Emacs you are using, will always be found first.  This
is useful when you install an experimental version of Emacs without
removing the standard installation.

If you want to override the order of directories in
`Info-default-directory-list', set INFOPATH in the environment.

If you run the Emacs executable from the `src' directory in the Emacs
source tree, and INFOPATH is not defined, the `info' directory in the
source tree is used as the first element of `Info-directory-list', in
place of the installation Info directory.  This is useful when you run
a version of Emacs without installing it.")


(defcustom Info-additional-directory-list nil
  "List of additional directories to search for Info documentation files.
These directories are searched after those in `Info-directory-list'."
  :type '(repeat directory)
  :group 'info)

(defcustom Info-scroll-prefer-subnodes nil
  "If non-nil, \\<Info-mode-map>\\[Info-scroll-up] in a menu visits subnodes.

If this is non-nil, and you scroll far enough in a node that its menu
appears on the screen, the next \\<Info-mode-map>\\[Info-scroll-up]
moves to a subnode indicated by the following menu item.  This means
that you visit a subnode before getting to the end of the menu.

Setting this option to nil results in behavior similar to the stand-alone
Info reader program, which visits the first subnode from the menu only
when you hit the end of the current node."
  :version "22.1"
  :type 'boolean
  :group 'info)

(defcustom Info-hide-note-references t
  "If non-nil, hide the tag and section reference in *note and * menu items.
If value is non-nil but not `hide', also replaces the \"*note\" with \"see\".
If value is non-nil but not t or `hide', the reference section is still shown.
nil completely disables this feature.  If this is non-nil, you might
want to set `Info-refill-paragraphs'."
  :version "22.1"
  :type '(choice (const :tag "No hiding" nil)
	  (const :tag "Replace tag and hide reference" t)
	  (const :tag "Hide tag and reference" hide)
	  (other :tag "Only replace tag" tag))
  :set (lambda (sym val)
	 (set sym val)
	 (dolist (buffer (buffer-list))
	   (with-current-buffer buffer
             (when (derived-mode-p 'Info-mode)
	       (revert-buffer t t)))))
  :group 'info)

(defcustom Info-refill-paragraphs nil
  "If non-nil, attempt to refill paragraphs with hidden references.
This refilling may accidentally remove explicit line breaks in the Info
file, so be prepared for a few surprises if you enable this feature.
This only has an effect if `Info-hide-note-references' is non-nil."
  :version "22.1"
  :type 'boolean
  :group 'info)

(defcustom Info-breadcrumbs-depth 4
  "Depth of breadcrumbs to display.
0 means do not display breadcrumbs."
  :version "23.1"
  :type 'integer
  :group 'info)

(defcustom Info-search-whitespace-regexp "\\s-+"
  "If non-nil, regular expression to match a sequence of whitespace chars.
This applies to Info search for regular expressions.
You might want to use something like \"[ \\t\\r\\n]+\" instead.
In the Customization buffer, that is `[' followed by a space,
a tab, a carriage return (control-M), a newline, and `]+'.  Don't
add any capturing groups into this value; that can change the
numbering of existing capture groups in unexpected ways."
  :type 'regexp
  :group 'info)

(defcustom Info-isearch-search t
  "If non-nil, isearch in Info searches through multiple nodes.
Before leaving the initial Info node, where isearch was started,
it fails once with the error message [end of node], and with
subsequent C-s/C-r continues through other nodes without failing
with this error message in other nodes.  When isearch fails for
the rest of the manual, it displays the error message [end of manual],
wraps around the whole manual and restarts the search from the top/final
node depending on search direction.

Setting this option to nil restores the default isearch behavior
with wrapping around the current Info node."
  :version "22.1"
  :type 'boolean
  :group 'info)

(defvar Info-isearch-initial-node nil)
(defvar Info-isearch-initial-history nil)
(defvar Info-isearch-initial-history-list nil)

(defcustom Info-mode-hook nil
  "Hook run when activating Info Mode."
  :type 'hook
  :group 'info)

(defcustom Info-selection-hook nil
  "Hook run when an Info node is selected as the current node."
  :type 'hook
  :group 'info)

(define-buffer-local Info-current-file nil
  "Info file that Info is now looking at, or nil.
This is the name that was specified in Info, not the actual file name.
It doesn't contain directory names or file name extensions added by Info.")

(define-buffer-local Info-current-subfile nil
  "Info subfile that is actually in the *info* buffer now.
It is nil if current Info file is not split into subfiles.")

(define-buffer-local Info-current-node nil
  "Name of node that Info is now looking at, or nil.")

(define-buffer-local Info-tag-table-marker nil
  "Marker pointing at beginning of current Info file's tag table.
Marker points nowhere if file has no tag table.")

(define-buffer-local Info-tag-table-buffer nil
  "Buffer used for indirect tag tables.")

(define-buffer-local Info-current-file-completions nil
  "Cached completion list for current Info file.")

(defvar Info-file-completions nil
  "Cached completion alist of visited Info files.
Each element of the alist is (FILE . COMPLETIONS)")

(define-buffer-local Info-file-supports-index-cookies nil
  "Non-nil if current Info file supports index cookies.")

(defvar Info-file-supports-index-cookies-list nil
  "List of Info files with information about index cookies support.
Each element of the list is a list (FILENAME SUPPORTS-INDEX-COOKIES)
where SUPPORTS-INDEX-COOKIES can be either t or nil.")

(define-buffer-local Info-index-alternatives nil
  "List of possible matches for last `Info-index' command.")

(defvar Info-point-loc nil
  "Point location within a selected node.
If string, the point is moved to the proper occurrence of the
name of the followed cross reference within a selected node.
If number, the point is moved to the corresponding line.")

(defvar Info-standalone nil
  "Non-nil if Emacs was started solely as an Info browser.")

(defvar Info-file-attributes nil
  "Alist of file attributes of visited Info files.
Each element is a list (FILE-NAME FILE-ATTRIBUTES...).")

(defvar Info-toc-nodes nil
  "Alist of cached parent-children node information in visited Info files.
Each element is (FILE (NODE-NAME PARENT SECTION CHILDREN) ...)
where PARENT is the parent node extracted from the Up pointer,
SECTION is the section name in the Top node where this node is placed,
CHILDREN is a list of child nodes extracted from the node menu.")

(defvar Info-index-nodes nil
  "Alist of cached index node names of visited Info files.
Each element has the form (INFO-FILE INDEX-NODE-NAMES-LIST).")

(defvar Info-virtual-files nil
  "List of definitions of virtual Info files.
Each element of the list has the form (FILENAME (OPERATION . HANDLER) EXTRA)
where FILENAME is a regexp that matches a class of virtual Info file names,
it should be carefully chosen to not cause file name clashes with
existing file names;
OPERATION is one of the symbols `find-file', `find-node', `toc-nodes';
and HANDLER is a function to call when OPERATION is invoked on a
virtual Info file.
EXTRA, if present, is one or more cons cells specifying extra
attributes important to some applications which use this data.
For example, desktop saving and desktop restoring use the `slow'
attribute to avoid restoration of nodes that could be expensive
to compute.")

(defvar Info-virtual-nodes nil
  "List of definitions of virtual Info nodes.
Each element of the list has the form (NODENAME (OPERATION . HANDLER) EXTRA)
where NODENAME is a regexp that matches a class of virtual Info node names,
it should be carefully chosen to not cause node name clashes with
existing node names;
OPERATION is the symbol `find-node';
and HANDLER is a function to call when OPERATION is invoked on a
virtual Info node.
EXTRA, if present, is one or more cons cells specifying extra
attributes important to some applications which use this data.
For example, desktop saving and desktop restoring use the `slow'
attribute to avoid restoration of nodes that could be expensive
to compute.")

(define-buffer-local Info-current-node-virtual nil
  "Non-nil if the current Info node is virtual.")

(defun Info-virtual-file-p (filename)
  "Check if Info file FILENAME is virtual."
  (Info-virtual-fun 'find-file filename nil))

(defun Info-virtual-fun (op filename nodename)
  "Find a function that handles operations on virtual manuals.
OP is an operation symbol (`find-file', `find-node' or `toc-nodes'),
FILENAME is a virtual Info file name, NODENAME is a virtual Info
node name.  Return a function found either in `Info-virtual-files'
or `Info-virtual-nodes'."
  (or (and (stringp filename) ; some legacy code can still use a symbol
	   (cdr-safe (assoc op (assoc-default filename
					      Info-virtual-files
					      'string-match))))
      (and (stringp nodename) ; some legacy code can still use a symbol
	   (cdr-safe (assoc op (assoc-default nodename
					      Info-virtual-nodes
					      'string-match))))))

(defun Info-virtual-call (virtual-fun &rest args)
  "Call a function that handles operations on virtual manuals."
  (when (functionp virtual-fun)
    (or (apply virtual-fun args) t)))

(defvar Info-suffix-list
  ;; The MS-DOS list should work both when long file names are
  ;; supported (Windows 9X), and when only 8+3 file names are available.
  (if (eq system-type 'ms-dos)
      '( (".gz"       . "gunzip")
	(".z"        . "gunzip")
	(".bz2"      . ("bzip2" "-dc"))
	(".inz"      . "gunzip")
	(".igz"      . "gunzip")
	(".info.Z"   . "gunzip")
	(".info.gz"  . "gunzip")
	("-info.Z"   . "gunzip")
	("-info.gz"  . "gunzip")
	("/index.gz" . "gunzip")
	("/index.z"  . "gunzip")
	(".inf"      . nil)
	(".info"     . nil)
	("-info"     . nil)
	("/index"    . nil)
	(""          . nil))
      '( (".info.Z"    . "uncompress")
	(".info.Y"    . "unyabba")
	(".info.gz"   . "gunzip")
	(".info.z"    . "gunzip")
	(".info.bz2"  . ("bzip2" "-dc"))
	(".info.xz"   . "unxz")
	(".info"      . nil)
	("-info.Z"    . "uncompress")
	("-info.Y"    . "unyabba")
	("-info.gz"   . "gunzip")
	("-info.bz2"  . ("bzip2" "-dc"))
	("-info.z"    . "gunzip")
	("-info.xz"   . "unxz")
	("-info"      . nil)
	("/index.Z"   . "uncompress")
	("/index.Y"   . "unyabba")
	("/index.gz"  . "gunzip")
	("/index.z"   . "gunzip")
	("/index.bz2" . ("bzip2" "-dc"))
	("/index.xz"  . "unxz")
	("/index"     . nil)
	(".Z"         . "uncompress")
	(".Y"         . "unyabba")
	(".gz"        . "gunzip")
	(".z"         . "gunzip")
	(".bz2"       . ("bzip2" "-dc"))
	(".xz"        . "unxz")
	(""           . nil)))
  "List of file name suffixes and associated decoding commands.
Each entry should be (SUFFIX . STRING); the file is given to
the command as standard input.

STRING may be a list of strings.  In that case, the first element is
the command name, and the rest are arguments to that command.

If STRING is nil, no decoding is done.
Because the SUFFIXes are tried in order, the empty string should
be last in the list.")

(defun info-insert-file-contents-1 (filename suffix lfn)
  (if lfn	; long file names are supported
      (concat filename suffix)
      (let* ((sans-exts (file-name-sans-extension filename))
	     ;; How long is the extension in FILENAME (not counting the dot).
	     (ext-len (max 0 (- (length filename) (length sans-exts) 1)))
	     ext-left)
	;; SUFFIX starts with a dot.  If FILENAME already has one,
	;; get rid of the one in SUFFIX (unless suffix is empty).
	(or (and (<= ext-len 0)
		 (not (eq (aref filename (1- (length filename))) #\.)))
	    (= (length suffix) 0)
	    (setq suffix (substring suffix 1)))
	;; How many chars of that extension should we keep?
	(setq ext-left (min ext-len (max 0 (- 3 (length suffix)))))
	;; Get rid of the rest of the extension, and add SUFFIX.
	(concat (substring filename 0 (- (length filename)
					 (- ext-len ext-left)))
		suffix))))

(defun info-file-exists-p (filename)
  (and (file-exists-p filename)
       (not (file-directory-p filename))))

(defun info-insert-file-contents (filename &optional visit)
  "Insert the contents of an Info file in the current buffer.
Do the right thing if the file has been compressed or zipped."
  (let* ((tail Info-suffix-list)
	 (lfn t) fullname decoder done)
    (elisp:if (info-file-exists-p filename)
	      ;; FILENAME exists--see if that name contains a suffix.
	      ;; If so, set DECODE accordingly.
	      (progn
		(while (and tail
			    (not (string-match
				  (concat (regexp-quote (car (car tail))) "$")
				  filename)))
		  (setq tail (cdr tail)))
		(setq fullname filename
		      decoder (cdr (car tail))))
	      ;; Try adding suffixes to FILENAME and see if we can find something.
	      (while (and tail (not done))
		(setq fullname (info-insert-file-contents-1 filename
							    (car (car tail)) lfn))
		(if (info-file-exists-p fullname)
		    (setq done t
			  ;; If we found a file with a suffix, set DECODER
			  ;; according to the suffix.
			  decoder (cdr (car tail))))
		(setq tail (cdr tail)))
	      (or tail
		  (error "Can't find ~a or any compressed version of it" filename)))
    ;; check for conflict with jka-compr
    (insert-file-contents fullname visit)
    ;; Clear the caches of modified Info files.
    (let* ((attribs-old (cdr (assoc fullname Info-file-attributes)))
	   (modtime-old (and attribs-old
			     (file-attribute-modification-time attribs-old)))
	   (attribs-new (and (stringp fullname) (file-attributes fullname)))
	   (modtime-new (and attribs-new
			     (file-attribute-modification-time attribs-new))))
      (when (and modtime-old modtime-new)
	(setq Info-index-nodes (remove (assoc (or Info-current-file filename)
					      Info-index-nodes)
				       Info-index-nodes))
	(setq Info-toc-nodes (remove (assoc (or Info-current-file filename)
					    Info-toc-nodes)
				     Info-toc-nodes)))
      ;; Add new modtime to `Info-file-attributes'.
      (setq Info-file-attributes
	    (cons (cons fullname attribs-new)
		  (remove (assoc fullname Info-file-attributes)
			  Info-file-attributes))))))

(defun Info-file-supports-index-cookies (&optional file)
  "Return non-nil value if FILE supports Info index cookies.
Info index cookies were first introduced in 4.7, and all later
makeinfo versions output them in index nodes, so we can rely
solely on the makeinfo version.  This function caches the information
in `Info-file-supports-index-cookies-list'."
  (or file (setq file Info-current-file))
  (or (assoc file Info-file-supports-index-cookies-list)
      ;; Skip virtual Info files
      (and (or (not (stringp file))
	       (Info-virtual-file-p file))
           (setq Info-file-supports-index-cookies-list
		 (cons (cons file nil) Info-file-supports-index-cookies-list)))
      (save-excursion
	  (let ((found nil))
	    (goto-char (point-min))
	    (unwind-protect ()
	      (if (and (re-search-forward
			"makeinfo[ \n]version[ \n]\\([0-9]+.[0-9]+\\)"
			(line-beginning-position 4) t)
		       (not (version< (match-string 1) "4.7")))
		  (setq found t))
	      (error nil))
	    (setq Info-file-supports-index-cookies-list
		  (cons (cons file found) Info-file-supports-index-cookies-list)))))
  (cdr (assoc file Info-file-supports-index-cookies-list)))

;;FIXME: Add a way to detect info directories
(defun Info-default-dirs ()
  '("/usr/local/share/info/" "/usr/share/info/" "/usr/local/share/info/"))

(defun info-initialize ()
  "Initialize `Info-directory-list', if that hasn't been done yet."
  (unless Info-directory-list
    (let ((path (getenv "INFOPATH")))
      ;; For a self-contained (ie relocatable) NS build, AFAICS we
      ;; always want the included info directory to be at the head of
      ;; the search path, unless it's already in INFOPATH somewhere.
      ;; It's at the head of Info-default-directory-list,
      ;; but there's no way to get it at the head of Info-directory-list
      ;; except by doing it here.
      (and path
	   (featurep 'ns)
	   (let ((dir (expand-file-name "../info" data-directory)))
	     (and (file-directory-p dir)
		  (not (member dir (split-string path ":")))
		  (push dir Info-directory-list)))))))

(defun info-setup (file-or-node buffer)
  (declare (ignore file-or-node buffer))
  "Display Info node FILE-OR-NODE in BUFFER."
  (Info-mode)
  ;;(if file-or-node
  ;; If argument already contains parentheses, don't add another set
  ;; since the argument will then be parsed improperly.  This also
  ;; has the added benefit of allowing node names to be included
  ;; following the parenthesized filename.
  ;; (Info-goto-node
  ;;  (if (and (stringp file-or-node) (string-match "(.*)" file-or-node))
  ;;      file-or-node
  ;; 	   (concat "(" file-or-node ")")))
  ;; (if (and (zerop (buffer-size))
  ;; 	       (null Info-history))
  ;; 	  ;; If we just created the Info buffer, go to the directory.
  ;; 	  (Info-directory)))
  )

(defcommand info-other-window ((&optional file-or-node)
			       (:file "Insert file: "))
  "Like `info' but show the Info buffer in another window."
  (info-setup file-or-node
	      (switch-to-buffer "*info*")))

(defvar info-mode-syntax-table
  (let ((st (make-syntax-table)))
    (modify-syntax-entry #\" :symbol-constituent :table st)
    (modify-syntax-entry #\\ :symbol-constituent :table st)
    ;; We add `p' so that M-c on 'hello' leads to 'Hello' rather than 'hello'.
    (modify-syntax-entry #\' :symbol-constituent :table st)
    st)
  "Syntax table used while in `Info-mode'.")

(defvar *Info-mode*
  (make-instance 'major-mode
		 :name "Info"
		 :syntax-table info-mode-syntax-table))

(defcommand Info-mode ()
  "Info mode provides commands for browsing through the Info documentation tree.
Documentation in Info is divided into \"nodes\", each of which discusses
one topic and contains references to other nodes which discuss related
topics.  Info has commands to follow the references and show you other nodes.

\\<Info-mode-map>\
\\[Info-help]	Invoke the Info tutorial.
\\[quit-window]	Quit Info: reselect previously selected buffer.

Selecting other nodes:
\\[Info-mouse-follow-nearest-node]
	Follow a node reference you click on.
	  This works with menu items, cross references, and
	  the \"next\", \"previous\" and \"up\", depending on where you click.
\\[Info-follow-nearest-node]	Follow a node reference near point, like \\[Info-mouse-follow-nearest-node].
\\[Info-next]	Move to the \"next\" node of this node.
\\[Info-prev]	Move to the \"previous\" node of this node.
\\[Info-up]	Move \"up\" from this node.
\\[Info-menu]	Pick menu item specified by name (or abbreviation).
	  Picking a menu item causes another node to be selected.
\\[Info-directory]	Go to the Info directory node.
\\[Info-top-node]	Go to the Top node of this file.
\\[Info-final-node]	Go to the final node in this file.
\\[Info-backward-node]	Go backward one node, considering all nodes as forming one sequence.
\\[Info-forward-node]	Go forward one node, considering all nodes as forming one sequence.
\\[Info-next-reference]	Move cursor to next cross-reference or menu item.
\\[Info-prev-reference]	Move cursor to previous cross-reference or menu item.
\\[Info-follow-reference]	Follow a cross reference.  Reads name of reference.
\\[Info-history-back]	Move back in history to the last node you were at.
\\[Info-history-forward]	Move forward in history to the node you returned from after using \\[Info-history-back].
\\[Info-history]	Go to menu of visited nodes.
\\[Info-toc]	Go to table of contents of the current Info file.

Moving within a node:
\\[Info-scroll-up]	Normally, scroll forward a full screen.
	  Once you scroll far enough in a node that its menu appears on the
	  screen but after point, the next scroll moves into its first
	  subnode.  When after all menu items (or if there is no menu),
	  move up to the parent node.
\\[Info-scroll-down]	Normally, scroll backward.  If the beginning of the buffer is
	  already visible, try to go to the previous menu entry, or up
	  if there is none.
\\[beginning-of-buffer]	Go to beginning of node.

Advanced commands:
\\[Info-search]	Search through this Info file for specified regexp,
	  and select the node in which the next occurrence is found.
\\[Info-search-case-sensitively]	Search through this Info file for specified regexp case-sensitively.
\\[isearch-forward], \\[isearch-forward-regexp]	Use Isearch to search through multiple Info nodes.
\\[Info-index]	Search for a topic in this manual's Index and go to index entry.
\\[Info-index-next]	(comma) Move to the next match from a previous \\<Info-mode-map>\\[Info-index] command.
\\[Info-virtual-index]	Look for a string and display the index node with results.
\\[info-apropos]	Look for a string in the indices of all manuals.
\\[Info-goto-node]	Move to node specified by name.
	  You may include a filename as well, as (FILENAME)NODENAME.
1 .. 9	Pick first ... ninth item in node's menu.
	  Every third `*' is highlighted to help pick the right number.
\\[Info-copy-current-node-name]	Put name of current Info node in the kill ring.
\\[clone-buffer]	Select a new cloned Info buffer in another window.
\\[universal-argument] \\[info]	Move to new Info file with completion.
\\[universal-argument] N \\[info]	Select Info buffer with prefix number in the name *info*<N>."
  (set-major-mode '*Info-mode*))


