(in-package "CEDAR")


(defvar *slynk* t
  "Create a slynk server")

;;FIXME: Improve this mode-line representation
(defun init-mode-line-format ()
  (setf *default-mode-line-format*
        (list "--:" ;; fake it for hype
              (lambda (buffer)
                (format nil "~C~C"
                        ;; FIXME: add read-only stuff
                        (if (buffer-modified-p buffer)
                            #\* #\-)
                        (if (buffer-modified-p buffer)
                            #\* #\-)))
              "  "
              (lambda (buffer)
                (format nil "~12,,,a" (buffer-name buffer)))
              "   "
              (lambda (buffer)
                (format nil "(~a)" 
                        (major-mode-name (symbol-value (buffer-major-mode buffer))))))))

(defun setup-scratch-buffer ()
  (set-buffer (get-buffer "*scratch*"))
  (insert *initial-scratch-message*)
  (set-major-mode '*lisp-interaction-mode*))

(defun setup-cedar-buffer ()
  (set-buffer (get-buffer "*CEDAR*"))
  (insert (format nil *format-initial-cedar-message* (uiop:getenv "USER")))
  (set-major-mode '*lisp-interaction-mode*))

(defun cedar ()
  "Run the CEDAR environment."
  (in-package :cedar)

  (unwind-protect
       (progn
	 (when *slynk*
	   (slynk:create-server :port 9887))
	 (setf *buffer-list* nil)
	 (init-mode-line-format)
	 (make-default-buffers)

	 (setup-scratch-buffer)
	 (setup-cedar-buffer)

	 ;; FIXME: is this a hack?
	 (setf (buffer-modified-p (current-buffer)) nil
	       (buffer-undo-list (current-buffer)) nil)

	 (init-command-arg-types)
	 (goto-char (point-min))
	 (setf *frame-list* (list (make-default-tty-frame
				   (get-buffer "*CEDAR*")
				   (init-tty)))
	       *selected-frame* (car *frame-list*)
	       *process-list* nil)
	 ;;(make-global-keymaps)
	 (catch 'cedar-quit 
	   (loop
	     (with-simple-restart (recursive-edit-top-level "Return to CEDAR top level")
	       (recursive-edit)))))))

(provide :cedar-0.1/main)
