(defpackage "ELISP"
  (:nicknames "EL")
  (:use :cl :alexandria :cl-ppcre)
  (:shadow cl:if cl:defun)
  (:export #:if #:defun))

(in-package "ELISP")

(defvar *parse-name-rules*
  '((cl-defun defun)
    (cl-incf incf)
    (cl-decf decf)
    (cl-defgeneric defgeneric)
    (cl-defmethod defmethod)))


(defclass elisp-sexp ()
  ((type :initarg :type :accessor elisp-type)
   (name :initarg :name :accessor elisp-name)
   (function-body :initarg :function-list :accessor elisp-body)))

(cl:defun clean-function-name (name)
  (cadr (find name *parse-name-rules* :test #'equal :key #'car)))

(defmethod parse-generic ((sexp elisp-sexp)))

(defgeneric seq-elt (sequence n)
  (:documentation "Return Nth element of SEQUENCE.")
  (:method (sequence n)
    (elt sequence n)))

(defmethod (setf seq-elt) (store (sequence cons) n)
  (setcar (nthcdr n sequence) store))

(defmethod (setf seq-elt) (store (sequence array) n)
  (aset sequence n store))

(cl:defun parse-file (file-name)
  (let* ((file-lines
	   (with-open-file (stream file-name)
	     (loop for line = (read-line stream nil)
		   while line
		   when (and (>= (length line) 1)
			     (not (char= #\; (char (string-trim " " line) 0))))
		   collect line)))
	 (file-string (concatenate
		       'string "(" (format nil "~{~a~}" file-lines) ")")))
    (read-from-string file-string)))

(defmacro if (test pass &rest else)
  "Elisp version of IF."
  `(cl:if ,test
          ,pass
          (progn 
            ,@else)))

(defmacro defun (name lambda-list &body body)
  "Parse an elisp style defun and convert it to a cl defun or lice defcommand."
  (let ((doc (when (stringp (car body))
               (pop body)))
        (decls (loop while (eq (caar body) 'declare)
		     collect (pop body)))
        (interactive (when (and (listp (car body))
                                (eq (caar body) 'interactive))
                       (pop body))))
    (if interactive
        `(defcommand ,name (,lambda-list
                            ,@(parse-interactive (cdr interactive)))
           ,@(append (list doc) decls body))
        `(cl:defun ,name ,@(append (list doc) decls body)))))
