;;; Lisp functions for making directory listings.

(in-package "CEDAR")

(defvar completion-ignored-extensions
  '(".o" "~" ".bin" ".lbin" ".so" ".a" ".ln" ".blg" ".bbl" ".elc" ".lof" ".glo" ".idx" ".lot" ".svn/" ".hg/" ".git/" ".bzr/" "CVS/" "_darcs/" "_MTN/" ".fmt" ".tfm" ".class" ".fas" ".lib" ".mem" ".x86f" ".sparcf" ".dfsl" ".pfsl" ".d64fsl" ".p64fsl" ".lx64fsl" ".lx32fsl" ".dx64fsl" ".dx32fsl" ".fx64fsl" ".fx32fsl" ".sx64fsl" ".sx32fsl" ".wx64fsl" ".wx32fsl" ".fasl" ".ufsl" ".fsl" ".dxl" ".lo" ".la" ".gmo" ".mo" ".toc" ".aux" ".cp" ".fn" ".ky" ".pg" ".tp" ".vr" ".cps" ".fns" ".kys" ".pgs" ".tps" ".vrs" ".pyc" ".pyo")
  "Completion ignores file names ending in any string in this list.
It does not ignore them if all possible completions end in one of
these strings or when displaying a list of completions.
It ignores directory names if they match any string in this list which
ends in a slash.")


(defstruct (file-attribute
	    (:print-function (lambda (struct stream depth)
			       (declare (ignore depth))
			       (format stream "~a" (file-attribute-format struct)))))
  (type nil :type boolean)
  (link-number 0 :type integer)
  (user-id "" :type string)
  (group-id "" :type string)
  (acces-time nil :type list) ; `current-time'
  (modification-time nil :type list)
  (status-change-time nil :type list)
  (size 0 :type integer)
  (modes "" :type string)
  (inode-number 0 :type integer)
  (device-number 0 :type integer))

(defun file-attribute-format (struct)
  (list (file-attribute-type struct)
	(file-attribute-link-number struct)
	(file-attribute-user-id struct)
	(file-attribute-group-id struct)
	(file-attribute-acces-time struct)
	(file-attribute-modification-time struct)
	(file-attribute-status-change-time struct)
	(file-attribute-size struct)
	(file-attribute-modes struct)
	(file-attribute-inode-number struct)
	(file-attribute-device-number struct)))



;;FIXME: Complete the supported types
(defun file-attributes (filename &optional id-format)
  "Return a list of attributes of file FILENAME.
Value is nil if specified file does not exist.

ID-FORMAT specifies the preferred format of attributes uid and gid (see
below) - valid values are string and integer.  The latter is the
default, but we plan to change that, so you should specify a non-nil value
for ID-FORMAT if you use the returned uid or gid.

To access the elements returned, the following access functions are
provided: file-attribute-type, file-attribute-link-number,
file-attribute-user-id, file-attribute-group-id,
file-attribute-access-time, file-attribute-modification-time,
file-attribute-status-change-time, file-attribute-size,
file-attribute-modes, file-attribute-inode-number, and
file-attribute-device-number.

Elements of the attribute list are:
 0. t for directory, string (name linked to) for symbolic link, or nil.
 1. Number of links to file.
 2. File uid as a string or (if ID-FORMAT is integer or a string value
  cannot be looked up) as an integer.
 3. File gid, likewise.
 4. Last access time, in the style of current-time.
  (See a note below about access time on FAT-based filesystems.)
 5. Last modification time, likewise.  This is the time of the last
  change to the file's contents.
 6. Last status change time, likewise.  This is the time of last change
  to the file's attributes: owner and group, access mode bits, etc.
 7. Size in bytes, as an integer.
 8. File modes, as a string of ten letters or dashes as in ls -l.
 9. An unspecified value, present only for backward compatibility.
10. inode number, as a nonnegative integer.
11. Filesystem device number, as an integer. "
  (declare (ignore id-format))
  (let ((is-directory (file-directory-p filename))
	(file-attributes (run-command
			  (format nil "~a ~a" "ls -liQ" filename))))
    (destructuring-bind (inode file-modes links
			 user group
			 byte-size month
			 day time
			 file-name)
	(split-string file-attributes)
      (declare (ignore month day time file-name))
      (file-attribute-format
       (make-file-attribute
	:type is-directory
	:link-number (parse-integer  links)
	:user-id  (run-command
		   (format nil "id --user ~a" user))
	:group-id group
	:size (parse-integer byte-size)
	:inode-number (parse-integer inode)
	:modes file-modes)))))

(defun directory-files (directory &optional)
  (mapcar #'pathname-name (uiop:directory-files directory )))

(defun directory-files-and-attributes ()
  (error "unimplemented directory-files-and-attributes"))

(defun file-name-completion ()
  (error "unimplemented file-name-completion"))

(defun file-name-all-completions ()
  (error "unimplemented file-name-all-completions"))


(defun file-attributes-lessp ()
  (error "unimplemented file-attributes-lessp"))
