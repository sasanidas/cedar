;;; cedar debugging facilities

(in-package "CEDAR")

(defun re-op-cedar (op)
  "Perform an asdf operation on :cedar and capture the output in a
buffer."
  (with-current-buffer (get-buffer-create "*cedar-reload*")
    (erase-buffer)
    (insert 
     (with-output-to-string (s)
       (let ((*debug-io* s)
	     (*error-output* s)
	     (*standard-output* s))
	 (asdf:oos op :cedar)))))
  (display-buffer "*cedar-reload*"))

(defcommand recompile-cedar ()
  (re-op-cedar 'asdf:compile-op))

(defcommand reload-cedar ()
  (re-op-cedar 'asdf:load-op))

(provide :cedar-0.1/debug)
