;;; SBCL
#+sbcl
(progn
  (require 'asdf)
  (require 'cedar))
#+sbcl 
(sb-ext:save-lisp-and-die "cedar" :toplevel (lambda ()
					      ;; asdf requires sbcl_home to be set, so set it to the value when the image was built
					      (sb-posix:putenv (format nil "SBCL_HOME=~A" #.(sb-ext:posix-getenv "SBCL_HOME")))
					      (cedar::cedar)
					      0)
				  :executable t)

;;; CLISP

;; asdf needs to be loaded. try putting (load "/path/to/asdf.lisp") in your .clisprc file
#+clisp
(asdf:oos 'asdf:load-op :cedar)

#+clisp
(progn
  (ext:saveinitmem "cedar" :init-function (lambda ()
					    (cedar::cedar)
					    (ext:quit))
			   :executable t :keep-global-handlers t :norc t :documentation "Lisp Computing Environment"))


#-(or sbcl clisp) (error "This lisp implementation is not supported.")
