;;; Implement the recursive edit.

(in-package "CEDAR")


;; TODO: Restore the window/buffer layout
(defun recursive-edit (&optional frame)
  (let* ((*last-command* *last-command*)
	 (current-frame (if frame frame
			    (window-frame (get-buffer-window (current-buffer)))))
	 (screen (frame-screen current-frame))
	 (ret (catch 'exit
		;;FIXME: Temporal non elegant solution
		;;With process-control-chars nil, C-x C-c doesn't fail
		(croatoan:with-screen (screen :process-control-chars nil
					      :input-blocking 100
					      :bind-debugger-hook nil)
		  (command-loop)
		  (croatoan:run-event-loop
		   (frame-screen current-frame))))))
    (when ret
      (signal 'quit))))

(provide :cedar-0.1/recursive-edit)
