;; File IO for CEDAR.
(in-package "CEDAR")

;; This file is part of CEDAR.

(defvar *file-name-handler-alist*
  '(("\\`\\(.+\\.\\(?:7z\\|CAB\\|LZH\\|MSU\\|ZIP\\|a\\(?:pk\\|r\\)\\|c\\(?:ab\\|pio\\)\\|de\\(?:b\\|pot\\)\\|exe\\|iso\\|jar\\|lzh\\|m\\(?:su\\|tree\\)\\|od[bfgpst]\\|pax\\|r\\(?:ar\\|pm\\)\\|shar\\|t\\(?:ar\\|bz\\|gz\\|lz\\|xz\\|zst\\)\\|warc\\|x\\(?:ar\\|p[is]\\)\\|zip\\)\\(?:\\.\\(?:Z\\|bz2\\|gz\\|l\\(?:rz\\|z\\(?:ma\\|[4o]\\)?\\)\\|uu\\|xz\\|zst\\)\\)*\\)\\(/.*\\)\\'" . tramp-archive-autoload-file-name-handler)
    ("\\.gpg\\(~\\|\\.~[0-9]+~\\)?\\'" . epa-file-handler)
    ("\\(?:\\.tzst\\|\\.zst\\|\\.dz\\|\\.txz\\|\\.xz\\|\\.lzma\\|\\.lz\\|\\.g?z\\|\\.\\(?:tgz\\|svgz\\|sifz\\)\\|\\.tbz2?\\|\\.bz2\\|\\.Z\\)\\(?:~\\|\\.~[-[:alnum:]:#@^._]+\\(?:~[[:digit:]]+\\)?~\\)?\\'" . jka-compr-handler)
    ("\\`/[^/|:]+:" . tramp-autoload-file-name-handler)
    ("\\`/:" . file-name-non-special)))

(defun find-file-name-handler (filename operation)
  (warn "Unimplemented find-file-name-handler"))

(defun file-name-nondirectory (filename)
  (pathname-name (pathname filename)))

(defun file-name-directory (filename)
  "Return the directory component in file name FILENAME.

Return nil if FILENAME does not include a directory.
Otherwise return a directory name."
  (let* ((directory (pathname filename))
	 (separator (uiop:directory-separator-for-host)))
    (when (pathname-directory directory)
      (subseq (namestring directory) 0
	      (1+ (position separator (namestring directory)
			    :test #'char= :from-end t))))))

(defun file-directory-p (filename)
  (if (uiop:directory-exists-p filename)
      t))

(defun file-exists-p (file)
  (uiop:file-exists-p file))

(defun file-name-as-directory (file)
  (when (stringp file)
    (let ((separator (uiop:directory-separator-for-host)))
      (format nil "~a~a" file separator))))

(defun directory-name-p (name)
  "Return non-nil if NAME is a directory"
  (uiop:directory-pathname-p (pathname name)))

(defun file-name-absolute-p (filename)
  "Return t if FILENAME is an absolute file name."
  (uiop:absolute-pathname-p (pathname filename)))

(defun expand-file-name (name &optional default-directory)
  (let ((separator (uiop:directory-separator-for-host))
	(default-directory (if default-directory
			       (if (file-name-absolute-p default-directory)
				   (pathname default-directory)
				   (uiop:truenamize default-directory)))))
    (format nil "~a~a~a" (namestring default-directory) separator name)))

(defun car-less-than-car (a b)
  "Return t if (car A) is numerically less than (car B)."
  (< (car a) (car b)))
