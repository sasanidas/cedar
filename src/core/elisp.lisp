;; It also uses eclector (https://github.com/s-expressionists/Eclector)
(defpackage emacs-lisp
  (:documentation "Emacs Lisp compatibility layer.")
  (:nicknames "EL")
  (:use :cl :alexandria)
  (:shadow defcustom defconst autoload defun if / member delete ignore
	   featurep load require provide))

(in-package :emacs-lisp)

(defconstant +elisp-pack+ (find-package :el)
  "The Emacs-Lisp package.")

(defun un-unspecific (value)
  "Convert :UNSPECIFIC to NIL."
  (if (eq value :unspecific) nil value))

(defun probe-directory-generic (path)
  (let ((probe (probe-file path)))
    (and probe
         (null (un-unspecific (pathname-name probe)))
         (null (un-unspecific (pathname-type probe))))))

(defun probe-directory (filename)
  "Check whether the file name names an existing directory."
  ;; based on
  ;; From: Bill Schelter <wfs@fireant.ma.utexas.edu>
  ;; Date: Wed, 5 May 1999 11:51:19 -0500
  ;; fold the name.type into directory
  (let* ((path (pathname filename))
         (name (un-unspecific (pathname-name path)))
         (type (un-unspecific (pathname-type path)))
         (new-dir
	   (cond ((and name type) (list (concatenate 'string name "." type)))
		 (name (list name))
		 (type (list type))
		 (t nil))))
    (when new-dir
      (setq path (make-pathname
                  :directory
                  (append (or (un-unspecific (pathname-directory path))
                              '(:relative))
                          new-dir)
                  :name nil :type nil :version nil :defaults path)))
    #+allegro (excl::probe-directory path)
    #+clisp (values (ignore-errors (ext:probe-directory path)))
    #+cmu (eq :directory (unix:unix-file-kind (namestring path)))
    #+lispworks (lw:file-directory-p path)
    #+sbcl
    (let ((s (or (find-symbol "UNIX-FILE-KIND" "SB-UNIX")
                 (find-symbol "NATIVE-FILE-KIND" "SB-IMPL"))))
      (if s
          (eq :directory (funcall s (namestring path)))
          (probe-directory-generic path)))
    #-(or allegro clisp cmu lispworks sbcl)
    (probe-directory-generic path)))

(defmacro el::if (ii tt &rest ee)
  "Emacs-Lisp version of `if' (multiple `else' clauses)."
  (cl:if ee `(cl:if ,ii ,tt (cl:progn ,@ee)) `(cl:if ,ii ,tt)))

(defmacro el::while (cc &body forms)
  "Emacs-Lisp `while'."
  `(do () ((not ,cc)) ,@forms))

(defun lalist-vars (lalist-elt)
  "Extract the variables from the lambda-list element."
  (cond ((consp lalist-elt)
         (let ((var (first lalist-elt))
               (predicate (third lalist-elt)))
           (when (consp var)
             (setq var (second var)))
           (if predicate
               (list var predicate)
               (list var))))
        ((cl:member lalist-elt lambda-list-keywords :test #'eq)
         ())
        (t (list lalist-elt))))

(defmacro el::defun (name args &body body)
  "Emacs-Lisp version of `defun' (all arguments special)."
  `(cl:defun ,name ,args ,@body))

(defun read-vector (stream char)
  (declare (cl:ignore char))
  (coerce
   (eclector.reader:read-delimited-list #\] stream t) 'vector))

(defun el::read-elisp-special (stream char)
  (declare (stream stream) (character char))
  (ecase char
    (#\[ (read-vector stream char ))
    (#\" (coerce
          (loop with char-to-add and next-char
                for this-char = (eclector.reader:read-char stream t nil t)
                until (char= this-char #\")
                if (char= this-char #\\)
                do (setq char-to-add
			 (case (setq next-char (eclector.reader:read-char stream t nil t))
			   (#\n #\Newline) (#\r #\Return) (#\f #\Page)
			   (#\t #\Tab) (#\v #\Linefeed) (t next-char)))
                else do (setq char-to-add this-char)
                collect char-to-add)
          'string))
    (#\? (loop for cc of-type character = (eclector.reader:read-char stream t nil t)
               collect (if (char/= cc #\\) cc
			   (ecase (eclector.reader:read-char stream t nil t)
			     (#\C (eclector.reader:read-char stream t nil t) :control)
			     (#\^ :control)
			     (#\S (eclector.reader:read-char stream t nil t) :shift)
			     (#\M (eclector.reader:read-char stream t nil t) :meta)
			     (#\s (eclector.reader:read-char stream t nil t) :super)
			     (#\H (eclector.reader:read-char stream t nil t) :hyper)
			     (#\n (setq cc #\Null) #\Newline)
			     (#\e (setq cc #\Null) #\Newline)
			     (#\t (setq cc #\Null) #\Tab)
			     (#\r (setq cc #\Null) #\Return)))
               into res
               finally (return (if (characterp (car res)) (car res) res))
               while (char= cc #\\)))))

(defvar *closio-method*  nil
  "The technique used for CLOS i/o.
Acceptable values are:
  nil         -- use the default (unreadable) technique - #<>.
  :slot-name  -- output each (bound) slot, by name (really readable: `read'
                 will return an `equal' object when `*print-readably*' is T)
  :initarg    -- output only the slots which have an initarg (user-controlled)
This variable should have the same value when reading and writing,
otherwise you will probably get an error.")

(defun read-object (st char arg)
  "Read an instance of a CLOS class printed as #[name{ slot val}]"
  (declare (cl:ignore char arg))
  (case *closio-method*
    (:slot-name
     (do* ((all (eclector.reader:read-delimited-list #\] st t)) (res (make-instance (car all)))
	   (tail (cdr all) (cddr tail)))
	  ((endp tail) res)
       (setf (slot-value res (car tail)) (cadr tail))))
    (:initarg (apply #'make-instance (eclector.reader:read-delimited-list #\] st t)))
    ((nil) (error 'code :proc 'read-object :args '(*closio-method* nil)
			:mesg "~s is ~s"))
    (t (error 'case-error :proc 'read-object :args
	      (list '*closio-method* *closio-method*
		    :initarg :slot-name nil)))))

(defun make-clos-readtable (&optional
			    (rt (eclector.readtable:copy-readtable eclector.reader:*readtable*)))
  "Return the readtable for reading #[]."
  (eclector.readtable:set-syntax-from-char #\[ #\( rt)
  (eclector.readtable:set-syntax-from-char #\] #\) rt)
  (eclector.readtable:set-macro-character rt #\]
					  (eclector.readtable:get-macro-character rt #\) ) nil )
  (eclector.readtable:set-dispatch-macro-character rt #\# #\[ #'read-object )
  rt)


(defun el::make-elisp-readtable ()
  "Make the readtable for Emacs-Lisp parsing."
  (let ((rt (make-clos-readtable)))

    (eclector.readtable:set-syntax-from-char #\[ #\()
    (eclector.readtable:set-syntax-from-char #\] #\))

    (eclector.readtable:set-macro-character rt #\[ #'read-vector nil)
    (eclector.readtable:set-macro-character rt #\]
					    (eclector.readtable:get-macro-character rt  #\)) nil)

    (eclector.readtable:set-macro-character rt #\? #'el::read-elisp-special  nil )
    (eclector.readtable:set-macro-character rt #\" #'el::read-elisp-special  nil )
    ;; (setf (readtable-case rt) :downcase)
    rt))

;; (defconstant +elisp-readtable+ (el::make-elisp-readtable)
;;   "The readtable for Emacs-Lisp parsing.")



(defun el::memq (elt list) (cl:member elt list :test #'eq))
(defun el::delq (elt list) (cl:delete elt list :test #'eq))
(defun el::member (elt list) (cl:member elt list :test #'equal))
(defun el::delete (elt list) (cl:delete elt list :test #'equal))
(defun el::concat (&rest args) (apply #'concatenate 'string args))
(defun el::put (symbol propname value) (setf (get symbol propname) value))
(defun el::fset (symbol def)
  (if (null def) (fmakunbound symbol)
      (setf (fdefinition symbol) (if (functionp def) def (fdefinition def)))))
(defun el::setcar (cons obj) (setf (car cons) obj))
(defun el::setcdr (cons obj) (setf (cdr cons) obj))
(defun el::ignore (&rest ignore) (declare (cl:ignore ignore)) nil)
(defun el::sit-for (sec &optional (ms 0) nodisp)
  (declare (real sec ms) (cl:ignore nodisp))
  (sleep (+ sec (/ ms 1000))))

(defun el::string-to-number (string &optional (base 10))
  (if (= base 10)
      (eclector.reader:read-from-string string)
      (parse-integer string :radix base)))

(defun el::/ (&rest args)
  (if (every #'integerp args)
      (reduce #'floor (cdr args) :initial-value (car args))
      (apply #'/ args)))

(defun el::decode-time (&optional (time (get-universal-time)))
  (unless (numberp time)
    (setq time (+ (ash (car time) 16)
                  (if (numberp (cdr time)) (cdr time) (cadr time)))))
  (multiple-value-bind (sec minute hour day month year dow dst zone)
      (decode-universal-time time)
    (list sec minute hour day month year (mod (1+ dow) 7) dst (* 3600 zone))))

(defun el::encode-time (second minute hour day month year &rest zone)
  (encode-universal-time second minute hour day month year (car (last zone))))

(defun el::defalias (symbol def)
  (eval `(defmacro ,symbol (&body body) (list* ',def body))))

(defmacro el::defgroup (&rest args) (declare (cl:ignore args)))

(defmacro el::defcustom (var val doc &key (type t) &allow-other-keys)
  (let ((type (if (and (consp type) (eq 'quote (car type))) (cadr type) type)))
    `(progn (declaim (type ,type ,var)) (defvar ,var ,val ,doc))))

(defmacro el::defface (name val doc &rest args)
  (declare (cl:ignore args))
  `(defvar ,name ,val ,doc))

(defmacro el::defconst (name val doc) `(defconstant ,name ,val ,doc))

(defmacro el::eval-when-compile (&rest body)
  `(eval-when (:compile-toplevel :load-toplevel) ,@body))

(defmacro el::setq-default (&rest body) `(setq ,@body))
(defmacro el::save-window-excursion (&rest body) `(progn ,@body))
(defmacro el::save-excursion (&rest body) `(progn ,@body))
(defmacro el::with-output-to-temp-buffer (&rest body) `(progn ,@body))

(defun el::number-to-string (number) (write-to-string number :radix 10))
(defun el::int-to-string (number) (el::number-to-string number))
(defun el::char-to-string (char) (string char))
(defun el::string-to-int (&rest args) (apply #'el::string-to-number args))
(defun el::file-truename (file) (truename file))
(defun el::file-exists-p (file) (and (probe-file file) t))
(defun el::substring (seq from &optional to) (subseq seq from to))

(defun el::% (x y) (rem x y))
(defun el::file-directory-p (ff) (probe-directory ff))
(defun el::sref (ar ix) (aref ar ix))
(defun el::set-default (sy va) (setf (symbol-value sy) va))
(defun el::default-value (sy) (symbol-value sy))

(defun el::run-hooks (&rest hooks)
  (labels ((rh (hook)
             (etypecase hook
               (list (dolist (hk hook) (rh hk)))
               ((or symbol function) (funcall hook)))))
    (dolist (hk hooks) (when (boundp hk) (rh (symbol-value hk))))))

(defun el::add-hook (hook function &optional append local)
  (declare (cl:ignore local))
  (let ((val (if (boundp hook) (symbol-value hook) nil)))
    (when (or (functionp val) (symbolp val)) (setq val (list val)))
    (unless (cl:member function val :test #'equal)
      (setf (symbol-value hook)
            (if append (append val (list function))
                (cons function val))))))

(defun el::remove-hook (hook function &optional local)
  (declare (cl:ignore local))
  (when (and (boundp hook) (symbol-value hook) function)
    (let ((val (symbol-value hook)))
      (if (or (functionp val) (symbolp val))
          (when (eq val function) (setf val nil))
          (setf (symbol-value hook) (remove function val :test #'equal))))))

(defun el::add-to-list (list el)
  (unless (boundp list) (setf (symbol-value list) nil))
  (pushnew el (symbol-value list) :test #'equal))

(defmacro defun-fake (func)
  `(defun ,func (&rest args)
     (warn "~s [~{~s~^ ~}]: not implemented yet" ',func args)
     (values-list args)))

(defun-fake el::define-key)
(defun-fake el::make-sparse-keymap)
(defun-fake el::substitute-key-definition)
(defun-fake el::interactive)
(defun-fake el::make-help-screen)
(defun-fake el::help-for-help)
(defun-fake el::start-kbd-macro)
(defun-fake el::substitute-command-keys)
(defun-fake el::display-color-p)
(defun-fake el::propertize)
(defun-fake el::make-mode-line-mouse-map)

(defvar el::global-map (el::make-sparse-keymap))
(defvar el::help-char (code-char 8))
(defvar el::help-form nil)

(defvar el::window-system nil)
(defvar el::mode-line-format nil)
(defvar el::buffer-read-only nil)
(defvar el::indent-tabs-mode nil)

(deftype el::sexp () 'list)
(deftype el::file () 'string)
(deftype el::hook () '(or list symbol))

(defun el::car-safe (l)
  (if (listp l)
      (car l)
      nil))

(defun el::mapconcat (function sequence separator)
  ;; (el::mapconcat #'identity '("a" "b" "c") " ") ==> "a b c"
  (apply #'concatenate 'string
         (cdr (mapcan (lambda (el) (list separator el))
                      (map 'list function sequence)))))

(defun to-directory (path)
  (if (char= #\/ (schar path (1- (length path)))) path
      (concatenate 'string path "/")))

(defun el::directory-files (directory &optional full match nosort)
  ;; incomplete
  (declare (cl:ignore full nosort))
  (let ((dir (to-directory directory)))
    (directory (if match (merge-pathnames match dir) dir))))


(defun default-directory ()
  "The default directory."
  #+allegro (excl:current-directory)
  #+clisp (ext:default-directory)
  #+cmu (ext:default-directory)
  #+cormanlisp (ccl:get-current-directory)
  #+lispworks (hcl:get-working-directory)
  #+lucid (lcl:working-directory)
  #-(or allegro clisp cmu cormanlisp lispworks lucid) (truename "."))

(defun el::expand-file-name (name &optional (default-dir (default-directory)))
  (namestring (merge-pathnames (to-directory default-dir) name)))

(defun el::autoload (function file &optional docstring interactive type)
  (declare (symbol function) (type (or simple-string null) docstring)
           (cl:ignore interactive type))
  (unless (fboundp function)
    (setf (fdefinition function)
          (lambda (&rest args)
            (setf (documentation function 'function) nil)
            (fmakunbound function)
            (format t "; ~s is being autoloaded from `~a'~%" function file)
            (el::load file)
            (apply function args))
          (documentation function 'function)
          (format nil "Autoloaded (from ~a)~@[:~%~a~]" file docstring))))

(defvar el::emacs-home
  #+(or mswindows win32) "d:/gnu/emacs/"
  #-(or mswindows win32) "/usr/local/share/emacs/27.2/")

(defvar el::site-lisp-dir
  #+(or mswindows win32) "c:/gnu/sitelisp/"
  #-(or mswindows win32) "/usr/local/share/emacs/site-lisp/")

(defvar el::load-path
  (list (concatenate 'string el::emacs-home "lisp/")
        (concatenate 'string el::emacs-home "leim/")
        el::site-lisp-dir))

(defvar el::features nil)

(defun el::featurep (feature) (el::memq feature el::features))


#+allegro (pushnew "el" sys:*source-file-types* :test #'equal)
#+cmu (pushnew "el" ext::*load-source-types* :test #'equal)
#+clisp (pushnew "el" custom:*source-file-types* :test #'equalp)
#+lispworks (pushnew "el" system:*text-file-types* :test #'equal)
#+gcl (error 'not-implemented :proc 'file-types)


(defun locate-file (file &optional source-only)
  (flet ((file-here (dir)
           ;; search the local files
           (let ((ff (merge-pathnames file dir)))
             (when (and (ignore-errors (probe-file ff))
                        (not (probe-directory ff)))
               (return-from locate-file ff)))
           (unless source-only
             (let ((ff (merge-pathnames (compile-file-pathname file) dir)))
               (when (probe-file ff) (return-from locate-file ff))))
           (let ((ff (merge-pathnames (concatenate 'string file ".el") dir)))
             (when (probe-file ff) (return-from locate-file ff)))))
    (dolist (path el::load-path)
      (let ((dir (to-directory path)))
        (file-here dir)
        ;; search the subdirectories
        (dolist (sub-dir (directory (concatenate 'string dir "*/")))
          (file-here sub-dir))))))

(defun el::load (file &optional noerror nomessage nosuffix must-suffix)
  "Emacs-Lisp load. The suffix stuff is ignored."
  (declare (cl:ignore nosuffix must-suffix nomessage))
  (let* ((eclector.readtable:*readtable* +elisp-readtable+) (*package* +elisp-pack+)
	 (ff (locate-file file))
	 (reader-list (and ff (eclector.reader:read-from-string
			       (read-file-into-string ff)))))
    (if reader-list (eval reader-list)
	(unless noerror (error "file `~a' not found" file)))))

(defun el::require (feature &optional file-name noerror)
  "Emacs-Lisp require"
  (unless (el::featurep feature)
    (let ((file (or file-name (string-downcase (string feature)))))
      (el::load file noerror)
      (assert (el::featurep feature) ()
              "loading `~a' failed to provide `~a'" file feature)))
  feature)


(defun compile-el-file (file)
  (let ((eclector.reader:*readtable* +elisp-readtable+) (cl:*package* +elisp-pack+)
        (ff (or (locate-file file t) file)))
    (compile-file ff)))

(defun el::provide (feature) (pushnew feature el::features))


;;(el::load "backquote")

;;(el::load "subr")


;; (defmacro defun (name lambda-list &body body)
;;   "Parse an elisp style defun and convert it to a cl defun or lice defcommand."
;;   (let ((doc (when (stringp (car body))
;;                (pop body)))
;;         (decls (loop while (eq (caar body) 'declare)
;; 		     collect (pop body)))
;;         (interactive (when (and (listp (car body))
;;                                 (eq (caar body) 'interactive))
;;                        (pop body))))
;;     (if interactive
;;         `(defcommand ,name (,lambda-list
;;                             ,@(parse-interactive (cdr interactive)))
;;            ,@(append (list doc) decls body))
;;         `(cl:defun ,name ,@(append (list doc) decls body)))))
