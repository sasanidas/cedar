;;; An interactive debugger for cedar

(in-package "CEDAR")

(defvar *debugger-mode*
  (make-instance 'major-mode
                 :name "Debugger"
                 :map  (let ((m (make-sparse-keymap)))
                         (define-key m (kbd "q") 'debugger-invoke-top-level-restart)
                         m)))
(defun debugger-mode ()
  "See `*debugger-mode*'"
  (set-major-mode '*debugger-mode*))


(defun enter-debugger (condition)
  "Create a debugger buffer, print the error and any active restarts."
  (when (and (typep condition 'user-break)
             (or *inhibit-quit*
                 *waiting-for-input*))
    (setf *quit-flag* t)
    (continue))
  ;; make sure we're not in the minibuffer
  (select-window (first (frame-window-list (selected-frame))))
  (with-current-buffer (get-buffer-create "*debugger*")
    (display-buffer (current-buffer))
    (erase-buffer)
    (set-major-mode '*debugger-mode*)
    (insert (format nil "Debugger~%~a~%~%~a~%~{~a~%~}" (backtrace-as-string)
		    condition (compute-restarts))))
  (recursive-edit))

(defmacro with-cedar-debugger (&body body)
  `(handler-case 
       (progn ,@body)
     (error (c)
       (enter-debugger c ))))

(defcommand debugger-invoke-top-level-restart ()
  (when (get-buffer "*debugger*")
    (kill-buffer (get-buffer "*debugger*"))))

(defcommand toggle-debug-on-error ()
  "Toggle whether to enter Lisp debugger when an error is signaled.
In an interactive call, record this option as a candidate for saving
by \"Save Options\" in Custom buffers."
  (setf *debug-on-error* (not *debug-on-error*)))

(defcommand toggle-debug-on-quit ()
  "Toggle whether to enter Lisp debugger when C-g is pressed.
In an interactive call, record this option as a candidate for saving
by \"Save Options\" in Custom buffers."
  (setf *debug-on-quit* (not *debug-on-quit*)))
