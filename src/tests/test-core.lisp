(in-package :cedar/tests)

(5am:def-suite :core-unit-tests
  :description "Core unit test suite.")

(5am:in-suite :core-unit-tests)


(5am:test :check-default-buffers
  (5am:is (and
	   (cedar:get-buffer "*messages*")
	   (cedar:get-buffer "*scratch*")
	   (cedar:get-buffer "*CEDAR*"))))

(5am:test :check-cedar-buffer
  (cedar::with-current-buffer (cedar:get-buffer "*CEDAR*")
    (5am:is
     (equal 'cedar::*lisp-interaction-mode*
	    (cedar::buffer-major-mode (cedar:current-buffer))))
    (5am:is
     (string= (format nil cedar::*format-initial-cedar-message*
		      (uiop:getenv "USER"))
	      (cedar::buffer-substring (cedar:point-min)
				       (cedar:point-max))))))

(5am:def-fixture :buffer-context ()
  (cedar::with-current-buffer (cedar::get-buffer-create "*5AM-TESTS*")
    (cedar::erase-buffer)
    (&body)))

(5am:test :buffer-properties
  (5am:with-fixture :buffer-context ()
    (let* ((content "This is a test content. New line,@")
	   (content-size (length content)))

      (cedar::insert content)
      (5am:is (= content-size
		 (cedar::buffer-size (cedar::current-buffer))))

      (5am:is (string= content
		       (cedar::buffer-string)))

      (5am:is (= (cedar::point) content-size))

      (cedar::erase-buffer)
      (5am:is (= 0 (cedar::buffer-size (cedar::current-buffer)))))))

(5am:test :buffer-movement
	  (5am:with-fixture :buffer-context ()
	    (let* ((content "This is a test content. New line,@")
		   (beginning-position 0)
		   (end-position 34))
	      (cedar::insert content)
	      (5am:is (= end-position (cedar::end-of-buffer)))

	      (5am:is (= beginning-position
			 (cedar::beginning-of-buffer))))))
