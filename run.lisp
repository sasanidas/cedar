(eval-when (:compile-toplevel)
  (error "This lisp file should be run interpreted."))

(let* ((cedar-location (directory-namestring *load-pathname*))
       (src-directory (merge-pathnames "src" cedar-location))
       (asdf:*central-registry* (list (format nil "~a~a" src-directory
					      (uiop:directory-separator-for-host))))
       (lib-directory (format nil "~a~a~a~a"
			      (namestring src-directory)
			      (uiop:directory-separator-for-host)
			      "lib"
			      (uiop:directory-separator-for-host)))
       (system-dependencies '("alexandria" "cl-ppcre" "uiop" "croatoan" "cluffer"))
       (dependencies-location (mapcar (lambda (dep)
					(format nil "~a~a" lib-directory dep))
				      system-dependencies)))
  (loop for dep-path in dependencies-location
	for dep-name in system-dependencies
	do (asdf:load-asd (concatenate
			   'string
			   (namestring (truename dep-path))
			   (concatenate 'string dep-name ".asd"))))

  (asdf:load-asd (format nil "~a~a~a" src-directory
			 (uiop:directory-separator-for-host)
			 "cedar.asd"))
  (asdf:load-system :cedar))

(cedar::cedar)
